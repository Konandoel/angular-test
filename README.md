<h1>Angular test app</h1>

<p>install packages</p>
<pre>
<code>npm install</code>
</pre>
<p>or</p>
<pre>
<code>yarn install</code>
</pre>
<hr>
<p>start server and watcher<p>

<pre>
<code>npm start</code>
</pre>

<p>or</p>

<pre>
<code>yarn start</code>
</pre>
<hr>
<p>start build</p>

<pre>
<code>npm build</code>
</pre>

<p>or</p>

<pre>
<code>yarn build</code>
</pre>