'use strict';

// Modules
const webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    autoprefixer = require('autoprefixer');

const ENV = process.env.npm_lifecycle_event,
    isProd = ENV === 'build';
    
const WebpackConfig = () => {
  let config = {};

  config.entry = {
    app: './src/app.js'
  }

  config.output = {
    path: __dirname + '/dist',

    publicPath: isProd ? '/' : 'http://localhost:8080/',

    filename: isProd ? '[name].[hash].js' : '[name].bundle.js',

    chunkFilename: isProd ? '[name].[hash].js' : '[name].bundle.js',
  }

  if (isProd) 
    config.devtool = 'source-map'
  else
    config.devtool = 'eval-source-map'  
  
  config.module = {
    rules: [{
      // JS LOADER
      // https://github.com/babel/babel-loader
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/
    },
    {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          { 
            loader: 'css-loader', 
            options: { 
              minimize: true || {zindex: false},
              soureMap: true 
            } 
          }
        ],
      })
    }, 
    {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          { 
            loader: 'css-loader', 
            options: { 
              minimize: true || {zindex: false}, 
              soureMap: true } 
          },
          { 
            loader: 'postcss-loader',
            options: {
              plugins: (loader) => [
                autoprefixer({
                  browsers: ['last 2 version', 'ie >= 9']
                })
              ]
            }
          },
          { 
            loader: 'sass-loader', 
            options: { 
              soureMap: true ,
              data: '@import "var";',
              includePaths: ['./src/public/css/']
            }
          }
        ]
      })
    },
    {
      test: /\.(png|jpg|jpeg|gif|svg)$/,
      loader: 'file-loader?name=images/[name].[ext]'
    },
    {
      test: /\.(woff|woff2|ttf|eot)$/,
      loader: 'file-loader?name=fonts/[name].[ext]'
    },
    {
      test: /\.html$/,
      loader: 'raw-loader'
    }]
  }

  config.plugins = [
    new HtmlWebpackPlugin({
      template: './src/public/index.html',
      inject: 'body'
    }),

    new ExtractTextPlugin({
      filename: 'css/[name].css',
      disable: !isProd,
      allChunks: true
    })
  ]

  if (isProd) {
    config.plugins.push(
      new webpack.NoErrorsPlugin(),

      new webpack.optimize.UglifyJsPlugin()
    );
  }

  config.devServer = {
    contentBase: './src/public'
  }

  return config;
}

module.exports = WebpackConfig;