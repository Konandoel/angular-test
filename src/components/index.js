import main from './main'
import search from './search'
import details from './details'
import author from './author'
import tags from './tags'


export default angular.module('components', [
    main,
    search,
    details,
    author,
    tags
  ])
  .name
