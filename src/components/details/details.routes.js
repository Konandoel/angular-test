routes.$inject = ['$stateProvider']

export default function routes($stateProvider) {
  $stateProvider
    .state('details', {
      url: '/details?{question_id}',
      controller: 'DetailsController',
      controllerAs: 'DetailsCtrl',
      template: require('./details.html'),
      params: {
        order: {
          value: 'desc',
          squash: true
        },
        sort: {
          value: 'votes',
          squash: true
        },
        page: {
          value: '1',
          squash: true
        },
        pagesize: {
          value: '10',
          squash: true
        }
      }
    });
}