import './style.scss'

import angular from 'angular'
import uirouter from 'angular-ui-router'

import routing from './details.routes'
import DetailsController from './details.controller'

export default angular.module('details', [uirouter])
  .config(routing)
  .controller('DetailsController', DetailsController)
  .name