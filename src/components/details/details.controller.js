class SearchController {
  constructor($state, $stateParams, details, $window) {
    this.state = $state;

    this.window = $window

    this.params = $stateParams;

    this.details = details;

    this.details.question(this.params.question_id)
      .then((result) => {
        this.questionDetails = result.data.items[0]
      });

    this.details.answers(this.params.question_id, {
      order: this.params.order,
      sort: this.params.sort,
      page: this.params.page,
      pagesize: this.params.pagesize
    }).then((result) => {
      this.answers = result.data.items
    });
  }

  goBack() {
    this.window.history.back()
  }

}

SearchController.$inject = [
  '$state',
  '$stateParams',
  'details',
  '$window'
]

export default SearchController


