routes.$inject = ['$stateProvider']

export default function routes($stateProvider) {
  $stateProvider
    .state('author', {
      url: '/author/{authorId}?order&sort&page&pagesize',
      parent: 'search',
      params: {
        order: {
          value: 'desc',
          squash: true
        },
        sort: {
          value: 'votes',
          squash: true
        },
        page: {
          value: '1',
          squash: true
        },
        pagesize: {
          value: '30',
          squash: true
        }
      },
      onEnter: ['$uibModal', '$state', ($uibModal, $state) => {
        var modalInstance = $uibModal.open({
          controller: 'AuthorController',
          controllerAs: 'AuthorCtrl',
          resolve: {
            params: ['$stateParams', ($stateParams) => {
                return $stateParams
            }]
          },
          template: require('./author.html')
        }).result.finally(() => {
            $state.go('^');
        });     
      }]
    });
}