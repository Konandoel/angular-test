import './style.scss'

import angular from 'angular'
import uirouter from 'angular-ui-router'

import routing from './author.routes'
import AuthorController from './author.controller'

export default angular.module('author', [uirouter])
  .config(routing)
  .controller('AuthorController', AuthorController)
  .name