class AuthorController {
  constructor(answers, $state, $stateParams, $timeout) {

    $timeout(() => {
      this.state = $state;
      
      this.params = $stateParams;
      
      this.answers = answers

      this.currentPage = _.toNumber(this.params.page);

      this.answers.searchResult(this.params.authorId, {
        order: this.params.order,
        sort: this.params.sort,
        page: 1,
        pagesize: 10
      }).then((result) => {
        this.answersList = result.data.items
      });
    })
  }

}

AuthorController.$inject = [
  'answers',
  '$state',
  '$stateParams',
  '$timeout'
]

export default AuthorController


