class MainController {
  constructor($state) {

    this.searchfieled = ''

    this.state = $state
  }

    search() {
      this.state.go('search', {intitle: this.searchfieled, page: 1})
    } 
}

MainController.$inject = [
  '$state'
]

export default MainController


