routes.$inject = ['$stateProvider']

export default function routes($stateProvider) {
  $stateProvider
    .state('main', {
      url: '/',
      controller: 'MainController',
      controllerAs: 'MainCtrl',
      template: require('./main.html')
    });
}