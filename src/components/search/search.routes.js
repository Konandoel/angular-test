routes.$inject = ['$stateProvider']

export default function routes($stateProvider) {
  $stateProvider
    .state('search', {
      url: 'search?order&sort&page&pagesize&{intitle}',
      controller: 'SearchController',
      controllerAs: 'SearchCtrl',
      template: require('./search.html'),
      parent: 'main',
      params: {
        order: {
          value: 'desc',
          squash: true
        },
        sort: {
          value: 'votes',
          squash: true
        },
        page: {
          value: '1',
          squash: true
        },
        pagesize: {
          value: '10',
          squash: true
        }
      }
    });
}