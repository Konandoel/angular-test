class SearchController {
  constructor($stateParams, search, $state) {

    this.state = $state;
    
    this.params = $stateParams;

    this.searchfieled = this.params.intitle;

    this.currentPage = _.toNumber(this.params.page);

    this.search = search;
    
    this.search.searchResult({
      intitle: this.params.intitle,
      order: this.params.order,
      sort: this.params.sort,
      page: this.params.page,
      pagesize: this.params.pagesize
    }).then((result) => {
      this.searchResult = result.data
    });
  }

  pageChange() {
    this.state.go('.', {page: this.currentPage});
  }

}

SearchController.$inject = [
  '$stateParams',
  'search',
  '$state'
]

export default SearchController


