import './style.scss'

import angular from 'angular'
import uirouter from 'angular-ui-router'

import routing from './search.routes'
import SearchController from './search.controller'

export default angular.module('search', [uirouter])
  .config(routing)
  .controller('SearchController', SearchController)
  .name