import './style.scss'

import angular from 'angular'
import uirouter from 'angular-ui-router'

import routing from './tags.routes'
import TagsController from './tags.controller'

export default angular.module('tags', [uirouter])
  .config(routing)
  .controller('TagsController', TagsController)
  .name