routes.$inject = ['$stateProvider']

export default function routes($stateProvider) {
  $stateProvider
    .state('tags', {
      url: '/tags?{tagged}&order&sort&page&pagesize',
      parent: 'search',
      params: {
        order: {
          value: 'desc',
          squash: true
        },
        sort: {
          value: 'votes',
          squash: true
        },
        page: {
          value: '1',
          squash: true
        },
        pagesize: {
          value: '10',
          squash: true
        }
      },
      onEnter: ['$uibModal', '$state', ($uibModal, $state) => {
        var modalInstance = $uibModal.open({
          controller: 'TagsController',
          controllerAs: 'TagsCtrl',
          template: require('./tags.html')
        }).result.finally(() => {
            $state.go('^');
        });     
      }]
    });
}