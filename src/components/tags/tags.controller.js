class TagsController {
  constructor($stateParams, search, $state, $timeout) {

    $timeout(() => {
      this.state = $state;
      
      this.params = $stateParams;

      this.searchfieled = this.params.intitle;

      this.currentPage = _.toNumber(this.params.page);

      this.search = search;
      
      this.search.searchResult({
        tagged: this.params.tagged,
        order: this.params.order,
        sort: this.params.sort,
        page: 1,
        pagesize: 10
      }).then((result) => {
        this.searchResult = result.data
      });
    })
  }


  pageChange() {
    this.state.go('.', {page: this.currentPage});
  }

}

TagsController.$inject = [
  '$stateParams',
  'search',
  '$state',
  '$timeout'
]

export default TagsController

