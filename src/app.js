const _ = require('lodash')

import 'bootstrap/dist/css/bootstrap.css'
import './public/css/style.scss'

import angular from 'angular'


import ngAnimate from 'angular-animate'
import ngSanitize from 'angular-sanitize'
import uirouter from 'angular-ui-router'
import uiBootstrap from 'angular-ui-bootstrap'

import routes from './app.config'
import components from './components'
import services from './services'

angular.module('app', [
    ngAnimate,
    ngSanitize,
    uirouter,    
    uiBootstrap,
    components,
    services
  ])
  .constant('api', 'http://api.stackexchange.com/2.2')
  .config(routes)