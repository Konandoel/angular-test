import angular from 'angular'

import search from './search'
import answers from './answer'
import details from './details'

// export services as module

export default angular.module('app.service', [])
  .service('search', search)
  .service('answers', answers)
  .service('details', details)
  .name