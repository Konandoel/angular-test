class details {
  constructor($http, api) {
    this.$http = $http;

    this.api = api;
  }

  question(question_id) {
    return this.$http.get( this.api + '/questions/' + question_id + '?site=stackoverflow&filter=!9YdnSIN18', {
      paramSerializer: '$httpParamSerializerJQLike',
      params: {
        order: 'desc',
        sort: 'votes'
      }
    })
  }

  answers(question_id, res) {
    return this.$http.get( this.api + '/questions/' + question_id + '/answers?filter=!9YdnSMKKT&site=stackoverflow', {
      paramSerializer: '$httpParamSerializerJQLike',
      params: res
    });
  };
}

details.$inject = [
  '$http',
  'api'
]


export default details
