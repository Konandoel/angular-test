class search {
  constructor($http, api) {
    this.$http = $http;

    this.api = api;
  }

  searchResult(res) {
    return this.$http.get(this.api + '/search?site=stackoverflow&filter=!9YdnSPuG8',{
      paramSerializer: '$httpParamSerializerJQLike',
      params: res
    });
  }
}

search.$inject = [
  '$http',
  'api'
]

export default search