class answers {
  constructor($http, api) {
    this.$http = $http;

    this.api = api;
  }

  searchResult(userId, res) {
    return this.$http.get(this.api + '/users/' + userId + '/answers?site=stackoverflow&filter=!-*f(6t0WW)1e', {
      paramSerializer: '$httpParamSerializerJQLike',
      params: res
    });
  }
}

answers.$inject = [
  '$http',
  'api'  
]

export default answers;